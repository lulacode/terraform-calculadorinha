terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "lulacode"

    workspaces {
      name = "terraform"
    }
  }
}

module "k8s-csd" {
  source = "./k8s-csd"
  token  = var.digitalocean_token
}

