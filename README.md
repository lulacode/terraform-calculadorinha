# Dojo de Terraform

Esse Dojo encontra-se nesta branch (_master_) já em seu estado final, ou seja, resolvido.

Ele foi criado para criar a infraestrutura necessárias na Digital Ocean para rodar o [Dojo de Kubernetes da K21](https://gitlab.com/knowledge21/csd/dojos/pt-br/kubernetes).

## Desafio

Dado que não há mais código para escrever neste Dojo, você pode usá-lo para dois tipos de desafio:

### Subir a infraestrutura na sua conta da Digital Ocean

Neste primeiro tipo de desafio você pode fazer um fork do projeto e configurar uma conta no [Terraform](https://app.terraform.io/) com um workspace apontando para este seu repositório e para a sua conta na Digital Ocean. Toda essa configuração pode ser feita pela própria interface web do Terraform.

O desafio é esse cluster de k8s ser criado automagicamente na sua conta da Digital Ocean diretamente desse seu workspace criado no Terraform.

>Dica: A variável de ambiente que o Terraform vai pedir é a TF_VAR_digitalocean_token, e ela deve ser configurada com [o API Token que você criar na sua conta da Digital Ocean](https://www.digitalocean.com/docs/apis-clis/api/create-personal-access-token/)_

### Adaptar o código para usar nomes customizados que você queira

O Cluster que está subindo aqui está com os nomes que a gente usa no nosso Dojo. Um desafio maneiro é você fazer um fork desse repo e trocar nomes para algo mais pessoal, que tenha a ver com você para ver o que acontece.

>Por exemplo: você pode trocar o nome do cluster de 'k8s-csd' para 'k8s-alfredinho'

Qualquer dúvida, chama nóis!

_[Twitter do Lula](https://twitter.com/luizphx)_

_[Twitter do Ayrton](https://twitter.com/ayrtonfreeman)_

Bj!